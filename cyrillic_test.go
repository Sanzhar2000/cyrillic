package cyrillic

import (
	"testing"
)

type words struct{
	field1 string
	field2 string
	field3 string
}

func TestNoCyrillic(t *testing.T) {
	var word words

	word.field1 = "qwertyйцукенqwerty"
	expectedData1 := "qwertyqwerty"
	res1 := NoCyrillic(word.field1)
	if len(res1) != len(expectedData1){
		t.Fatalf("got:\n%s\nexpected:\n%s", res1, expectedData1)
	}else {
		for i := 0; i < len(res1); i++ {
			if res1[i] != expectedData1[i] {
				t.Fatalf("got:\n%s\nexpected:\n%s", res1, expectedData1)
			}
		}
	}

	word.field2 = "ОдинДваТриOneTwoThree"
	expectedData2 := "OneTwoThree"
	res2 := NoCyrillic(word.field2)
	if len(res2) != len(expectedData2){
		t.Fatalf("got:\n%s\nexpected:\n%s", res2, expectedData2)
	}else {
		for i := 0; i < len(res2); i++ {
			if res2[i] != expectedData2[i] {
				t.Fatalf("got:\n%s\nexpected:\n%s", res2, expectedData2)
			}
		}
	}

	word.field3 = "ЗаСинимМоремOneThingIDontKnowWhy"
	expectedData3 := "OneThingIDontKnowWhy"
	res3 := NoCyrillic(word.field3)
	if len(res3) != len(expectedData3){
		t.Fatalf("got:\n%s\nexpected:\n%s", res3, expectedData3)
	}else {
		for i := 0; i < len(res3); i++ {
			if res3[i] != expectedData3[i] {
				t.Fatalf("got:\n%s\nexpected:\n%s", res3, expectedData3)
			}
		}
	}
}