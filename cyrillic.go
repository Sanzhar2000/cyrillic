package cyrillic

import "regexp"

func NoCyrillic(str string) string {
	reg := regexp.MustCompile("[\u0401\u0451\u0410-\u044f]")
	str = reg.ReplaceAllString(str, "")

	return str;
}
